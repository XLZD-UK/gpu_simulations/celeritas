# Steps
1. Install spack
2. Install celertias env
3. Install Celeritas


## Spack
Instructions from: https://spack.readthedocs.io/en/latest/getting_started.html

```bash
dnf install epel-release
dnf group install "Development Tools"
dnf install curl findutils gcc-gfortran gnupg2 hostname iproute redhat-lsb-core python3 python3-pip python3-setuptools unzip python3-boto3

git clone -c feature.manyFiles=true https://github.com/spack/spack.git
. spack/share/spack/setup-env.sh
```

## Celeritas Environment
Instruction from: https://celeritas-project.github.io/celeritas/user/main/installation.html

```bash


```

Spack environment for Celeritas
```yaml
spack:
  specs: 
    - cmake
    - doxygen
    - "geant4@11 cxxstd=17"
    - git
    - git-lfs
    - "googletest@1.10:"
    - hepmc3
    - ninja
    - nlohmann-json
    - mpi
    - "python@3.6:"
    - py-breathe
    - py-furo
    - py-sphinx
    - py-sphinxcontrib-bibtex
    - "root@6.24: cxxstd=17"
    - "swig@4.1:"
    - "vecgeom@1.2.4: +gdml cxxstd=17"
  view: true
  concretizer:
    unify: true
  packages:
    root:
      # Note: ~gsl and ~math are removed because dd4hep requires them
      variants: ~aqua ~davix ~examples ~opengl ~x ~tbb
    all:
      providers:
        blas: [openblas]
        lapack: [openblas]
        mpi: [openmpi]
      # Uncomment to enable cuda build or run within the spack env:
      # spack config add packages:all:variants:"cxxstd=17 +cuda cuda_arch=<ARCH>"
      variants: cxxstd=17 # +cuda cuda_arch=<ARCH>

```


mounts

```bash
-v /usr/lib/wsl/drivers/nvdm.inf_amd64_57bf7efd38fef1c8/libnvoptix_loader.so.1:/usr/lib/wsl/drivers/nvdm.inf_amd64_57bf7efd38fef1c8/libnvoptix_loader.so.1 -v /usr/lib/wsl/drivers/nvdm.inf_amd64_57bf7efd38fef1c8/nvoptix.bin:/usr/
lib/wsl/drivers/nvdm.inf_amd64_57bf7efd38fef1c8/nvoptix.bin -v /usr/lib/wsl/drivers/nvdm.inf_amd64_57bf7efd38fef1c8/nvoptix.dll:/usr/lib/wsl/drivers/nvdm.inf_amd64_57bf7efd38fef1c8/nvoptix.dll -v /usr/lib/wsl/lib/libnvoptix.so.1
:/usr/lib/wsl/lib/libnvoptix.so.1 -v /usr/lib/wsl/lib/libnvoptix_loader.so.1:/usr/lib/wsl/lib/libnvoptix_loader.so.1 -v /usr/lib/x86_64-linux-gnu/dri/kms_swrast_dri.so:/usr/lib/x86_64-linux-gnu/dri/kms_swrast_dri.so -v /usr/lib/x86_64-linux-gnu/dri/swrast_dri.so:/usr/lib/x86_64-linux-gnu/dri/swrast_dri.so

```